package com.seren.v1;

import com.codeborne.selenide.ElementsCollection;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static junit.framework.TestCase.assertEquals;

/**
 *
 * Created by Herold on 17.07.2016.
 */
public class GoogleSearchTest {

    @Test
    public void testSearchAndFollowLink(){
        open("http://google.com/ncr");

        $(By.name("q")).setValue("Selenium automates browsers").pressEnter();
        assertCount(10);

        assertResultText(0, "Selenium - Web Browser Automation");
        followLink(0);

        $("#mainContent").shouldBe(visible);
        assertEquals("http://www.seleniumhq.org/",url());

    }

    private ElementsCollection results = $$(".srg>.g");

    public void assertCount(int count){
        results.shouldBe(size(count));
    }

    public void assertResultText( int index, String text){
        results.get(index).shouldHave(text(text));
    }

    public void followLink(int index){
        results.get(index).$(".r a").click();
    }

}
